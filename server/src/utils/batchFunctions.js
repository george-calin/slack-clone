// import { Op } from 'sequelize';

// NOT WORKING FOR SOME REASON
const userBatcher = async (ids, models) => {
  // const results = await models.User.findAll({ where: { id: ids } }, { raw: true });
  const results = await models.sequelize.query('SELECT * FROM "Users" WHERE id IN (:id)', {
    model: models.User,
    replacements: {
      id: ids,
    },
    raw: true,
  });

  const data = {};

  // group by user id
  results.forEach((r) => {
    if (data[r.id]) {
      data[r.id].push(r);
    } else {
      data[r.id] = [r];
    }
  });
  return ids.map((id) => (data[id] || null));
};

const teamBatcher = async (ids, models) => {
  const results = await models.sequelize.query(`
  SELECT *
  FROM "Teams" t
  INNER JOIN "Members" m
  ON t.id = m.team_id
  WHERE m.user_id IN (:user_id)`,
  {
    model: models.Team,
    replacements: {
      user_id: ids,
    },
    raw: true,
  });
  const data = {};

  // group by user id
  results.forEach((r) => {
    if (data[r.user_id]) {
      data[r.user_id].push(r);
    } else {
      data[r.user_id] = [r];
    }
  });
  return ids.map((id) => (data[id] || null));
};

const channelBatcher = async (ids, models, user) => {
  const data = {};

  const results = await models.sequelize.query(`
  SELECT * 
  FROM "Channels" c
  WHERE c.team_id IN (:team_id)
  AND ( 
    c.public = true OR 
    EXISTS ( 
      SELECT 1
      FROM "PcMembers" pc
      WHERE pc.channel_id = c.id
      AND pc.user_id = :user_id
    )
  )`, {
    model: models.Channel,
    replacements: {
      user_id: user.id,
      team_id: ids,
    },
    raw: true,
  });

  // group by team
  results.forEach((r) => {
    if (data[r.team_id]) {
      data[r.team_id].push(r);
    } else {
      data[r.team_id] = [r];
    }
  });

  return ids.map((id) => (data[id] || null));
};

const memberBatcher = async (ids, models) => {
  const data = {};
  const results = await models.sequelize.query(`
    SELECT U.*,
      M.team_id
    FROM "Users" U
    JOIN "Members" M
    ON U.id = M.user_id 
    AND M.team_id IN (:team_id)
  `, {
    model: models.User,
    replacements: {
      team_id: ids,
    },
    raw: true,
  });

  // group by team
  results.forEach((r) => {
    if (data[r.team_id]) {
      data[r.team_id].push(r);
    } else {
      data[r.team_id] = [r];
    }
  });

  return ids.map((id) => (data[id] || null));
};

const pcMemberBatcher = async (ids, models, user) => {
  const data = {};

  const results = await models.sequelize.query(`
    SELECT
      PC.user_id as id,
      PC.channel_id
    FROM "PcMembers" PC
    WHERE PC.channel_id IN (:channel_id)
    AND PC.user_id <> :user_id 
  `, {
    model: models.PcMember,
    replacements: {
      user_id: user.id,
      channel_id: ids,
    },
    raw: true,
  });

  // group by channel_id
  results.forEach((r) => {
    if (data[r.channel_id]) {
      data[r.channel_id].push(r);
    } else {
      data[r.channel_id] = [r];
    }
  });

  return ids.map((id) => (data[id] || null));
};

export {
  memberBatcher, pcMemberBatcher, channelBatcher, teamBatcher, userBatcher,
};
