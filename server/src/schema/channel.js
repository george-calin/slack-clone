import { gql } from 'apollo-server-express';

// type Channel {
//   id: Int!
//   name: String!
//   public: Boolean!
//   dm: Boolean!
//   messages: [Message!]!
//   users: [User!]!
// }

export default gql`
  type Channel {
    id: Int!
    name: String!
    public: Boolean!
    dm: Boolean!
    pcMembers: [User!]
  }

  type ChannelResponse {
    ok: Boolean!
    channel: Channel
    errors: [Error!]
  }

  type DMChannelResponse {
    id: Int!
    name: String!
  }

  type Mutation {
    createChannel(
      team_id: Int!
      name: String!
      public: Boolean = false
      dm: Boolean = false
      members: [Int!]
    ): ChannelResponse!
    createChannelDM(
      team_id: Int!
      members: [Int!]
    ): DMChannelResponse!
  }
`;
