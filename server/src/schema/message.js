import { gql } from 'apollo-server-express';

// for type Message the user shoud be user: User! but [User!]! its a hack for dataloader to work
export default gql`
  type Message {
    id: Int!
    text: String!
    user: [User!]!
    channel: Channel!
    createdAt: String!
    url: String
    filetype: String
  }

  type File {
    filename: String!
    mimetype: String!
    encoding: String!
  }

  type Subscription {
    newChannelMessage(channel_id: Int!): Message!
  }

  type Query {
    messages(channel_id: Int!, cursor: String): [Message!]!
  }

  type Mutation {
    createMessage(channel_id: Int!, text: String, file: Upload): Boolean!
    singleUpload(channel_id: Int!, file: Upload!): File!
  }
`;
