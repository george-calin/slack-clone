import { gql } from 'apollo-server-express';

export default gql`
  type Team {
    id: Int!
    name: String!
    isAdmin: Boolean!
    channels: [Channel!]!
    members: [User!]!
  }

  type CreateTeamResponse {
    ok: Boolean!
    team: Team
    errors: [Error!]
  }

  type Query {
    allTeams: [Team!]!
    inviteTeams: [Team!]!
    getTeamMembers(team_id: Int!): [User!]!
  }

  type VoidResponse {
    ok: Boolean!
    errors: [Error!]
  }

  type Mutation {
    createTeam(name: String): CreateTeamResponse!
    addTeamMember(email: String!, team_id: Int!): VoidResponse!
  }
`;
