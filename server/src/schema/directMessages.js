import { gql } from 'apollo-server-express';

export default gql`
  type DirectMessage {
    id: Int!
		text: String!
    sender: User!
    receiver_id: Int!
    createdAt: String!
  }

  type Subscription {
    newDirectMessage(team_id: Int!, user_id: Int!): DirectMessage!
  }

  type Query {
    directMessages(team_id: Int!, other_user_id: Int!): [DirectMessage!]!
  }

  type Mutation {
    createDirectMessage(receiver_id: Int!, team_id: Int!, text: String!): Boolean!
  }
`;
