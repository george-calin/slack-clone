export default (sequelize, DataTypes) => {
  const DirectMessage = sequelize.define('DirectMessage', {
    text: {
      type: DataTypes.STRING,
    },
  });

  DirectMessage.associate = function (models) {
    // 1:M
    DirectMessage.belongsTo(models.Team, {
      foreignKey: 'team_id',
    });
    DirectMessage.belongsTo(models.User, {
      foreignKey: 'receiver_id',
    });
    DirectMessage.belongsTo(models.User, {
      foreignKey: 'sender_id',
    });
  };

  return DirectMessage;
};
