export default (sequelize, DataTypes) => {
  const Channel = sequelize.define('Channel', {
    name: {
      type: DataTypes.STRING,
    },
    public: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    dm: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  });

  Channel.associate = (models) => {
    // 1:M
    Channel.belongsTo(models.Team, {
      foreignKey: 'team_id',
    });

    // N to M
    Channel.belongsToMany(models.User, {
      through: 'channel_member',
      foreignKey: 'channel_id',
    });

    // N to M
    Channel.belongsToMany(models.User, {
      through: models.PcMember,
      foreignKey: 'channel_id',
    });
  };

  return Channel;
};
