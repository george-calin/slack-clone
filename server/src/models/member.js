export default (sequelize, DataTypes) => {
  const Member = sequelize.define('Member', {
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  });

  return Member;
};
