export default (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    text: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    url: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
    filetype: {
      type: DataTypes.STRING,
      defaultValue: '',
    },
  }, {
    indexes: [
      {
        fields: ['createdAt'],
      },
    ],
  });

  Message.associate = function (models) {
    // 1:M
    Message.belongsTo(models.User, {
      foreignKey: 'user_id',
    });
    Message.belongsTo(models.Channel, {
      foreignKey: 'channel_id',
    });
  };

  return Message;
};
