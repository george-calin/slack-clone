import { Op } from 'sequelize';


const createResolver = (resolver) => {
  const baseResolver = resolver;
  baseResolver.createResolver = (childResolver) => {
    const newResolver = async (parent, args, context, info) => {
      await resolver(parent, args, context, info);
      return childResolver(parent, args, context, info);
    };
    return createResolver(newResolver);
  };
  return baseResolver;
};

export const requiresAuth = createResolver((parent, args, { user }) => {
  if (!user || !user.id) {
    throw new Error('Not authenticated');
  }
});

export const requiresTeamAccess = createResolver(async (parent, { channel_id }, { user, models }) => {
  if (!user || !user.id) {
    throw new Error('Not authenticated');
  }
  // check if part of the team
  const channel = await models.Channel.findOne({ where: { id: channel_id } });
  const member = await models.Member.findOne({
    where: { team_id: channel.team_id, user_id: user.id },
  });
  if (!member) {
    throw new Error("You have to be a member of the team to subcribe to it's messages");
  }
});

export const directMessageSubscribtion = createResolver(async (parent, { team_id, user_id }, { user, models }) => {
  if (!user || !user.id) {
    throw new Error('Not authenticated');
  }

  const nrMembers = await models.Member.count({
    where: {
      team_id,
      [Op.or]: [{ user_id }, { user_id: user.id }],
    },
  });

  if (nrMembers !== 2) {
    throw new Error('Something went wrong');
  }
});
