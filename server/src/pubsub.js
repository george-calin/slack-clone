import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';

const { REDIS_DOMAIN_NAME, PORT_NUMBER, REDIS_HOST } = process.env;

const options = {
  host: REDIS_DOMAIN_NAME,
  port: PORT_NUMBER,
  retryStrategy: (times) => Math.min(times * 50, 2000),
};

const pubsub = new RedisPubSub({
  publisher: new Redis(options),
  subscriber: new Redis(options),
});

export default pubsub;
