import formatErrors from '../utils/formatErrors';
import { requiresAuth } from '../permissions';
import { sequelize } from '../models';

export default {
  Channel: {
    pcMembers: (parent, args, { pcMemberLoader }) => pcMemberLoader.load(parent.id),
  },
  Mutation: {
    createChannel: requiresAuth.createResolver(async (parent, args, { models, user }) => {
      try {
        const member = await models.Member.findOne(
          { where: { user_id: user.id, team_id: args.team_id } },
          { raw: true },
        );

        if (!member.isAdmin) {
          return {
            ok: false,
            errors: [
              {
                path: 'name',
                message: 'You have to be the owner of the team to create channels',
              },
            ],
          };
        }

        const response = await sequelize.transaction(async (transaction) => {
          const channel = await models.Channel.create(args, { transaction });
          if (!args.public) {
            const { members } = args; // args.members.filter((m) => m !== user.id);
            members.unshift(user.id);
            // eslint-disable-next-line max-len
            await models.PcMember.bulkCreate(members.map((m) => ({ user_id: m, channel_id: channel.dataValues.id })), { transaction });
          }
          return channel;
        });

        return {
          ok: true,
          channel: response,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    createChannelDM: requiresAuth.createResolver(async (parent, args, { models, user }) => {
      const { members, team_id } = args;
      const allMembers = [user.id, ...members];
      // check if dm channel already exists with these members
      const [data, result] = await models.sequelize.query(`
        SELECT c.id, MAX(c.name) as name
        FROM "Channels" c 
        JOIN "PcMembers" pc
        ON c.id = pc.channel_id
        WHERE c.team_id = :team_id
        AND c.dm = true 
        AND c.public = false
        GROUP BY c.id
        HAVING ARRAY_AGG(pc.user_id) @> Array[:members]
        AND COUNT(pc.user_id) = :count_members
       `,
      {
        replacements: {
          team_id,
          members: allMembers,
          count_members: allMembers.length,
        },
        raw: true,
      });
      // if channel was found, return its id
      if (data.length) {
        return {
          id: data[0].id,
          name: data[0].name,
        };
      }

      const users = await models.User.findAll({
        raw: true,
        where: {
          id: members,
        },
      });

      const name = users.map((u) => u.username).join(', ');

      const channelId = await sequelize.transaction(async (transaction) => {
        const channel = await models.Channel.create({
          name,
          public: false,
          dm: true,
          team_id,
        }, { transaction });
        const cId = channel.dataValues.id;
        // eslint-disable-next-line max-len
        await models.PcMember.bulkCreate(allMembers.map((m) => ({ user_id: m, channel_id: cId })), { transaction });
        return cId;
      });
      return {
        id: channelId,
        name,
      };
    }),
  },
};
