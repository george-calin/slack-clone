import formatErrors from '../utils/formatErrors';
import { requiresAuth } from '../permissions';
import { tryLogin } from '../auth';

export default {
  User: {
    // teams: (parent, args, { models }) => (
    //   models.sequelize.query('SELECT * FROM "Teams" t INNER JOIN "Members" m ON t.id = m.team_id WHERE m.user_id = :user_id', {
    //     model: models.Team,
    //     replacements: {
    //       user_id: parent.id,
    //     },
    //     raw: true,
    //   })
    // ),
    teams: (parent, args, { teamLoader }) => teamLoader.load(parent.id),
  },
  Query: {
    allUsers: (parent, args, { models }) => models.User.findAll(),
    me: requiresAuth.createResolver((parent, args, { models, user }) => (
      models.User.findOne({ where: { id: user.id } }))),
    getUser: (parent, { user_id }, { models }) => models.User.findOne({ where: { id: user_id } }),
  },
  Mutation: {
    register: async (parent, args, { models }) => {
      try {
        const user = await models.User.create(args);
        return {
          ok: true,
          user,
        };
      } catch (err) {
        const errors = formatErrors(err, models);
        return {
          ok: false,
          errors,
        };
      }
    },
    login: (parent, { email, password }, { models, SECRET, SECRET2 }) => tryLogin(email, password, models, SECRET, SECRET2),
  },
};
