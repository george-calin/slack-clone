import { Op } from 'sequelize';
import { withFilter } from 'graphql-subscriptions';
import { requiresAuth, directMessageSubscribtion } from '../permissions';

import pubsub from '../pubsub';

const NEW_DIRECT_MESSAGE = 'NEW_DIRECT_MESSAGE';

export default {
  DirectMessage: {
    sender: (parent, args, { models }) => {
      if (parent.sender) {
        return parent.sender;
      }
      return models.User.findOne({ where: { id: parent.sender_id } }, { raw: true });
    },
  },
  Query: {
    directMessages: requiresAuth.createResolver(
      async (parent, { team_id, other_user_id }, { user, models }) => models.DirectMessage.findAll(
        {
          order: [['createdAt', 'ASC']],
          where: {
            team_id,
            [Op.or]: [
              {
                [Op.and]: [{ receiver_id: other_user_id, sender_id: user.id }],
              },
              {
                [Op.and]: [{ receiver_id: user.id, sender_id: other_user_id }],
              },
            ],
          },
        },
        { raw: true },
      )
      ,
    ),
  },
  Mutation: {
    createDirectMessage: requiresAuth.createResolver(
      async (parent, args, { models, user }) => {
        try {
          const message = await models.DirectMessage.create({
            ...args,
            sender_id: user.id,
          });
          pubsub.publish(NEW_DIRECT_MESSAGE, {
            team_id: args.team_id,
            user_id: args.user_id,
            sender_id: user.id,
            receiver_id: args.receiver_id,
            newDirectMessage: {
              ...message.dataValues,
              sender: {
                username: user.username,
              },
            },
          });
          return true;
        } catch (err) {
          return false;
        }
      },
    ),
  },
  Subscription: {
    newDirectMessage: {
      subscribe: directMessageSubscribtion.createResolver(withFilter(
        () => pubsub.asyncIterator(NEW_DIRECT_MESSAGE),
        (payload, args, { user }) => payload.team_id === args.team_id && (
          (payload.sender_id === user.id && payload.receiver_id === args.user_id) ||
          (payload.sender_id === args.user_id && payload.receiver_id === user.id)
        ),
      )),
    },
  },
};
