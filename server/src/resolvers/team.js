import formatErrors from '../utils/formatErrors';
import { requiresAuth } from '../permissions';
import { sequelize } from '../models';

export default {
  Team: {
    channels: (parent, args, { channelLoader }) => channelLoader.load(parent.id),
    members: (parent, args, { memberLoader }) => memberLoader.load(parent.id),
  },
  Query: {
    /* eslint no-trailing-spaces: "off" */
    /* eslint implicit-arrow-linebreak: "off" */
    getTeamMembers: requiresAuth.createResolver(async (parent, { team_id }, { user, models }) => 
      models.sequelize.query(`
        SELECT * 
        FROM "Users" u
        WHERE u.id <> :user_id
        AND EXISTS (
          SELECT 1
          FROM "Members" m
          WHERE u.id = m.user_id
          AND m.team_id = :team_id
        )`, {
        model: models.User,
        replacements: {
          team_id,
          user_id: user.id,
        },
        raw: true,
      })),
  },
  Mutation: {
    createTeam: requiresAuth.createResolver(
      async (parent, args, { models, user }) => {
        try {
          const response = await sequelize.transaction(async (transaction) => {
            const team = await models.Team.create({ ...args });
            await models.Channel.create(
              {
                name: 'general',
                public: true,
                team_id: team.id,
              },
              { transaction },
            );
            await models.Member.create(
              {
                team_id: team.id,
                user_id: user.id,
                isAdmin: true,
              },
              { transaction },
            );
            return team;
          });
          return {
            ok: true,
            team: response,
          };
        } catch (err) {
          const errors = formatErrors(err, models);
          return {
            ok: false,
            errors,
          };
        }
      },
    ),
    addTeamMember: requiresAuth.createResolver(
      async (parent, args, { models, user }) => {
        const { email, team_id } = args;

        try {
          const memberPromise = models.Member.findOne(
            { where: { team_id, user_id: user.id } },
            { raw: true },
          );
          const userPromise = models.User.findOne(
            { where: { email } },
            { raw: true },
          );

          const [member, userToAdd] = await Promise.all([
            memberPromise,
            userPromise,
          ]);

          if (!member.isAdmin) {
            return {
              ok: false,
              errors: [
                {
                  path: 'email',
                  message: 'You cannot add members to the team',
                },
              ],
            };
          }

          if (!userToAdd) {
            return {
              ok: false,
              errors: [
                {
                  path: 'email',
                  message: 'Could not find user with this email adress!',
                },
              ],
            };
          }

          await models.Member.create({
            user_id: userToAdd.id,
            team_id,
          });

          return {
            ok: true,
          };
        } catch (err) {
          return {
            ok: false,
            errors: formatErrors(err, models),
          };
        }
      },
    ),
  },
};
