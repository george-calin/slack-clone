import express from 'express';
import cors from 'cors';
import jwt from 'jsonwebtoken';
import path from 'path';
import DataLoader from 'dataloader';
import { createServer } from 'http';
import { ApolloServer } from 'apollo-server-express';

import models from './models';
import typeDefs from './typeDefs';
import resolvers from './resolvers';
import { refreshTokens } from './auth';
import {
  channelBatcher, teamBatcher, userBatcher, memberBatcher, pcMemberBatcher,
} from './utils/batchFunctions';

require('dotenv').config();

const { PORT, SECRET, SECRET2 } = process.env;
const app = express();
app.use(cors('*'));
app.use('/uploads', express.static(path.join(__dirname, '../uploads')));

const addUser = async (req, res, next) => {
  const token = req.headers['x-token'];
  if (token) {
    try {
      const { user } = jwt.verify(token, SECRET);
      req.user = user;
    } catch (err) {
      const refreshToken = req.headers['x-refresh-token'];
      const newTokens = await refreshTokens(
        token,
        refreshToken,
        models,
        SECRET,
        SECRET2,
      );
      if (newTokens.token && newTokens.refreshToken) {
        // res.set('Access-Control-Expose-Headers', '*');
        res.set('Access-Control-Expose-Headers', 'x-token, x-refresh-token');
        res.set('x-token', newTokens.token);
        res.set('x-refresh-token', newTokens.refreshToken);
      }
      req.user = newTokens.user;
    }
  }

  next();
};

app.use(addUser);

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req, connection }) => ({
    models,
    user: connection ? connection.context.user : req.user,
    // user: req.user,
    SECRET,
    SECRET2,
    userLoader: new DataLoader((ids) => userBatcher(ids, models)),
    teamLoader: new DataLoader((ids) => teamBatcher(ids, models)),
    channelLoader: new DataLoader((ids) => channelBatcher(ids, models, req.user)),
    memberLoader: new DataLoader((ids) => memberBatcher(ids, models)),
    pcMemberLoader: new DataLoader((ids) => pcMemberBatcher(ids, models, req.user)),
    serverUrl: req && `${req.protocol}://${req.get('host')}`,
  }),
  subscriptions: {
    onConnect: async (connectionParams, webSocket) => {
      const { token, refreshToken } = connectionParams;
      if (token && refreshToken) {
        try {
          const { user } = jwt.verify(token, SECRET);
          return { models, user };
        } catch (err) {
          const newTokens = await refreshTokens(token, refreshToken, models, SECRET, SECRET2);
          return { models, user: newTokens.user };
        }
      }

      return { models };
    },
  },
});

server.applyMiddleware({ app });

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);

models.sequelize.sync({ }).then(() => {
  // app.listen({ port: PORT }, () => console.log(
  //   `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`,
  // ));

  // ⚠️ Pay attention to the fact that we are calling `listen` on the http server variable
  // and not on `app`.
  httpServer.listen(PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`);
    // eslint-disable-next-line no-console
    console.log(`🚀 Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
  });
});
