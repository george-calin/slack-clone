import path from 'path';
import moment from 'moment';
import { Op } from 'sequelize';
import { createWriteStream } from 'fs';
import { withFilter } from 'graphql-subscriptions';
import { requiresAuth, requiresTeamAccess } from '../permissions';
import pubsub from '../pubsub';

const NEW_CHANNEL_MESSAGE = 'NEW_CHANNEL_MESSAGE';
const { PORT, LIMIT } = process.env;

export default {
  Message: {
    // url: (parent) => (parent.url ? `http://localhost:${PORT}/${parent.url}` : ''),
    url: (parent, args, { serverUrl }) => (parent.url ? `${serverUrl}/${parent.url}` : ''),
    // user: (parent, args, { models }) => models.User.findOne({ where: { id: parent.user_id } }, { raw: true }),
    user: (parent, args, { userLoader }) => userLoader.load(parent.user_id),
  },
  Query: {
    messages: requiresAuth.createResolver(

      async (parent, { channel_id, cursor }, { user, models }) => {
        const channel = await models.Channel.findOne({ raw: true, where: { id: channel_id } });
        if (!channel.public) {
          const member = await models.PcMember.findOne({
            where: { channel_id: channel.id, user_id: user.id },
            raw: true,
          });
          if (!member) {
            throw new Error('Not Authorized');
          }
        }
        const options = {
          order: [['createdAt', 'DESC']],
          where: { channel_id },
          limit: 1 * LIMIT,
        };

        if (cursor) {
          options.where.createdAt = {
            [Op.lt]: moment.unix((1 * cursor) / 1000).format(),
          };
        }

        return models.Message.findAll(
          options,
          { raw: true },
        );
      },
    ),
  },
  Mutation: {
    createMessage: requiresAuth.createResolver(
      async (parent, { file, ...args }, { models, user }) => {
        try {
          const messageData = args;
          if (file) {
            const {
              createReadStream, filename, mimetype, encoding,
            } = await file;
            await new Promise((res) => createReadStream()
              .pipe(
                createWriteStream(
                  path.join(__dirname, '../../uploads/', filename),
                ),
              )
              .on('close', res));
            messageData.filetype = mimetype;
            messageData.url = `../uploads/${filename}`;
          }
          const message = await models.Message.create({
            ...messageData,
            user_id: user.id,
          });
          message.dataValues.createdAt = moment(message.dataValues.createdAt).format('x');
          pubsub.publish(NEW_CHANNEL_MESSAGE, {
            channel_id: args.channel_id,
            newChannelMessage: {
              ...message.dataValues,
            },
          });
          return true;
        } catch (err) {
          console.log(err);
          return false;
        }
      },
    ),
    singleUpload: requiresAuth.createResolver(
      async (parent, args, { models, user }) => {
        const {
          createReadStream, filename, mimetype, encoding,
        } = await args.file;
        await new Promise((res) => createReadStream()
          .pipe(
            createWriteStream(
              path.join(__dirname, '../../uploads/', filename),
            ),
          )
          .on('close', res));
        return { filename, mimetype, encoding };
      },
    ),
  },
  Subscription: {
    newChannelMessage: {
      subscribe: requiresTeamAccess.createResolver(withFilter(
        () => pubsub.asyncIterator(NEW_CHANNEL_MESSAGE),
        (payload, args) => payload.channel_id === args.channel_id,
      )),
    },
  },
};
