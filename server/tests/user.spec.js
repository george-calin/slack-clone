import axios from 'axios';
// const axios = require('axios');
import 'regenerator-runtime/runtime';
import { XMLHttpRequest } from 'xmlhttprequest';

describe('user resolvers', () => {
  test('allUsers', async () => {
    const response = await axios.post('http://localhost:8080/graphql', {
      query: `
      query {
        allUsers {
          id
          username
          email
        }
      }
      `,
    });

    const { data } = response;
    expect(data).toMatchObject({
      data: {
        allUsers: [
          {
            id: 1,
            username: 'tony1234',
            email: 'tony1234@gmail.com',
          },
        ],
      },
    });
  });

  test('login', async () => {
    const response = await axios.post('http://localhost:8080/graphql', {
      mutation: `
      mutation{
        login(email: "tony1234@gmail.com", password: "parola") {
          token
          refreshToken
          ok
        }
      }
      `,
    });

    const { data: { login: { token, refreshToken } } } = response;

    const response2 = axios.post('http://localhost:8080/graphql', {
      mutation: `
      mutation  {
        createTeam(name: "team1") {
          ok
          team {
            name
          }
        }
      }
      `,
    }, {
      'x-token': token,
      'x-refresh-token': refreshToken,
    });

    expect(response2.data).toMatchObject({
      data: {
        createTeam: {
          ok: true,
          team: {
            name: 'team1',
          },
        },
      },
    });
  });
});
