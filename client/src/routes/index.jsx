import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import {
  Home, Register, Login, CreateTeam, ViewTeam,
} from '../pages';

export default () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
      <PrivateRoute path="/create-team" exact component={CreateTeam} />
      <PrivateRoute
        path="/view-team/:teamId?/:channelId?"
        exact
        component={ViewTeam}
      />
    </Switch>
  </BrowserRouter>
);
