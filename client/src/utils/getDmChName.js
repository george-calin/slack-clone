
import _ from 'lodash';

const getDmChName = (pcMembers, teamMembers) => (
  pcMembers.map(({ id }) => {
    const member = _.find(teamMembers, { id });
    return member && member.username;
  }).join(', ')
);

export default getDmChName;
