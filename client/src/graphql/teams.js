import { gql } from 'apollo-boost';

const ME_QUERY = gql`
  {
    me {
      id
      username
      teams {
        id
        name
        isAdmin
        channels {
          id
          name
          dm
          pcMembers {
            id
          }
        }
        members {
          id
          username
          email
        }
      }
    }
  }
`;

const ME_DIRECT_MESSAGE_QUERY = gql`
  query($user_id: Int!) {
    getUser(user_id: $user_id) {
      username
    }
    me {
      id
      username
      teams {
        id
        name
        isAdmin
        channels {
          id
          name
        }
      }
    }
  }
`;

const TEAM_MEMBERS_QUERY = gql`
query($team_id: Int!) {
  getTeamMembers(team_id: $team_id) {
    id
    username
    email
  }   
}
`;

export { ME_QUERY, ME_DIRECT_MESSAGE_QUERY, TEAM_MEMBERS_QUERY };
