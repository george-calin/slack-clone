/* eslint-disable react/prop-types */
import React from 'react';
import { extendObservable } from 'mobx';
import { observer } from 'mobx-react';
import {
  Form, Container, Input, Header, Message,
} from 'semantic-ui-react';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';
import { wsLink } from '../apollo';

class Login extends React.Component {
  constructor(props) {
    super(props);

    extendObservable(this, {
      email: '',
      password: '',
      aErrors: {},
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this[name] = value;
    this.aErrors[`${name}Error`] = undefined;
  };

  onSubmit = async () => {
    const { email, password } = this;
    const response = await this.props.mutate({
      variables: { email, password },
    });
    const {
      ok, token, refreshToken, errors,
    } = response.data.login;
    if (ok) {
      localStorage.setItem('token', token);
      localStorage.setItem('refreshToken', refreshToken);
      // refresh websocket
      wsLink.subscriptionClient.tryReconnect();
      this.props.history.push('/view-team');
    } else {
      const aErrors = {};
      errors.forEach(({ path, message }) => {
        aErrors[`${path}Error`] = message;
      });
      this.aErrors = aErrors;
    }
  };

  render() {
    const { email, password, aErrors } = this;
    const { emailError, passwordError } = aErrors || {};
    const { onChange, onSubmit } = this;

    const errorList = [];
    if (emailError) {
      errorList.push(emailError);
    }
    if (passwordError) {
      errorList.push(passwordError);
    }
    return (
      <Container>
        <Header as="h2">Login</Header>
        <Form>
          <Form.Field error={!!emailError}>
            <Input
              onChange={onChange}
              name="email"
              value={email}
              fluid
              icon="search"
              placeholder="Email..."
            />
          </Form.Field>
          <Form.Field error={!!passwordError}>
            <Input
              onChange={onChange}
              name="password"
              value={password}
              fluid
              icon="search"
              placeholder="Password..."
              type="password"
            />
          </Form.Field>
          <button onClick={onSubmit} className="ui primary button">
            Submit
          </button>
        </Form>
        {errorList.length ? (
          <Message
            error
            header="There was some errors with your submission"
            list={errorList}
          />
        ) : null}
      </Container>
    );
  }
}

const LOGIN_MUTATION = gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ok
      token
      refreshToken
      errors {
        path
        message
      }
    }
  }
`;

export default graphql(LOGIN_MUTATION)(observer(Login));
