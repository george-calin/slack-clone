import React from 'react';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';
// import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  Container, Form, Input, Header, Message,
} from 'semantic-ui-react';

const REGISTER_MUTATION = gql`
  mutation Register($username: String!, $email: String!, $password: String!) {
    register(username: $username, email: $email, password: $password) {
      ok
      errors {
        path
        message
      }
    }
  }
`;

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      usernameError: '',
      emailError: '',
      passwordError: '',
    };
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
      [`${name}Error`]: '',
    });
  };

  onSubmit = async (e) => {
    // e.preventDefault();
    this.setState({
      usernameError: '',
      emailError: '',
      passwordError: '',
    });
    const { username, email, password } = this.state;
    const response = await this.props.mutate({
      variables: { username, email, password },
    });

    const { ok, errors } = response.data.register;

    if (ok) {
      this.props.history.push('/login');
    } else {
      const oErrors = {};
      errors.forEach(({ path, message }) => {
        oErrors[`${path}Error`] = message;
      });
      this.setState(oErrors, function (cb) {
        console.log(this.state);
      });
    }
  };

  render() {
    const {
      username,
      email,
      password,
      usernameError,
      emailError,
      passwordError,
    } = this.state;
    const { onChange, onSubmit } = this;
    const errorList = [];
    if (usernameError) {
      errorList.push(usernameError);
    }
    if (emailError) {
      errorList.push(emailError);
    }
    if (passwordError) {
      errorList.push(passwordError);
    }
    return (
      <Container>
        <Header as="h2">Register</Header>
        <Form>
          <Form.Field error={!!usernameError}>
            <label>Username</label>
            <Input
              onChange={onChange}
              name="username"
              value={username}
              fluid
              icon="search"
              placeholder="Username..."
            />
          </Form.Field>
          <Form.Field error={!!emailError}>
            <label>Email</label>
            <Input
              onChange={onChange}
              name="email"
              value={email}
              fluid
              icon="search"
              placeholder="Email..."
            />
          </Form.Field>
          <Form.Field error={!!passwordError}>
            <label>Password</label>
            <Input
              onChange={onChange}
              name="password"
              value={password}
              fluid
              icon="search"
              placeholder="Password..."
              type="password"
            />
          </Form.Field>
          <button onClick={onSubmit} className="ui primary button">
            Submit
          </button>
        </Form>
        {errorList.length ? (
          <Message
            error
            header="There was some errors with your submission"
            list={errorList}
          />
        ) : null}
      </Container>
    );
  }
}

export default graphql(REGISTER_MUTATION)(Register);
