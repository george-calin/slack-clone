import Home from './Home';
import Login from './Login';
import Register from './Register';
import CreateTeam from './CreateTeam';
import ViewTeam from './ViewTeam';

export {
  Home, Login, Register, CreateTeam, ViewTeam,
};
