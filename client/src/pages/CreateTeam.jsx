/* eslint-disable react/prop-types */
import React from 'react';
import { extendObservable } from 'mobx';
import { observer } from 'mobx-react';
import {
  Form, Container, Input, Header, Message,
} from 'semantic-ui-react';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';

class CreateTeam extends React.Component {
  constructor(props) {
    super(props);

    extendObservable(this, {
      name: '',
      aErrors: {},
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this[name] = value;
    this.aErrors[`${name}Error`] = undefined;
  };

  onSubmit = async () => {
    const { name } = this;
    let response = null;
    try {
      response = await this.props.mutate({
        variables: { name },
      });
    } catch (errors) {
      this.props.history.push('/login');
      return;
    }

    const { ok, errors, team } = response.data.createTeam;

    if (ok) {
      this.props.history.push(`/view-team/${team.id}`);
    } else {
      const err = {};
      errors.forEach(({ path, message }) => {
        err[`${path}Error`] = message;
      });
      this.aErrors = err;
    }
  };

  render() {
    const { name, aErrors } = this;
    const { nameError } = aErrors || {};
    const { onChange, onSubmit } = this;

    const errorList = [];
    if (nameError) {
      errorList.push(nameError);
    }

    return (
      <Container>
        <Header as="h2">Create a team</Header>
        <Form>
          <Form.Field error={!!nameError}>
            <Input
              onChange={onChange}
              name="name"
              value={name}
              fluid
              icon="search"
              placeholder="Email..."
            />
          </Form.Field>
          <button onClick={onSubmit} className="ui primary button">
            Submit
          </button>
        </Form>
        {errorList.length ? (
          <Message
            error
            header="There was some errors with your submission"
            list={errorList}
          />
        ) : null}
      </Container>
    );
  }
}

const CREATE_TEAM_MUTATION = gql`
  mutation($name: String!) {
    createTeam(name: $name) {
      ok
      team {
        id
      }
      errors {
        path
        message
      }
    }
  }
`;

export default graphql(CREATE_TEAM_MUTATION)(observer(CreateTeam));
