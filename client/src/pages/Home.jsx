import React from "react";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

const allUsersQuery = gql`
  {
    allUsers {
      id
      email
    }
  }
`;

const Home = (props) => {
  const { data } = useQuery(allUsersQuery);
  const { allUsers = [] } = data || {};
  return allUsers.map((user) => <h1 key={user.id}>{user.email}</h1>);
};

export default Home;
