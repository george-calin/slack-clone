/* eslint-disable react/prop-types */
import React from 'react';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import { gql } from 'apollo-boost';

import { Redirect } from 'react-router-dom';
import { AppLayout, Header, SendMessage } from '../components/dumb';
import { MessageContainer } from '../components/containers';
import Sidebar from '../components/containers/Sidebar';

import { ME_QUERY } from '../graphql/teams';
import { getDmChName } from '../utils';

const ViewTeam = ({
  match: { params },
  data: { loading, me },
  mutate,
}) => {
  const { teamId, channelId } = params;

  if (loading || !me) {
    return null;
  }

  const { teams } = me;

  if (!teams.length) {
    return <Redirect to="/create-team" />;
  }
  if (teamId !== undefined && Number.isNaN(teamId)) {
    return <Redirect to="/view-team" />;
  }

  const teamIdx = teamId ?
    teams.findIndex((item) => item.id === 1 * teamId) :
    0;

  const currentTeam = teamIdx === -1 ? teams[0] : teams[teamIdx];
  const channelIdInteger = 1 * channelId;
  const channelIdx = channelIdInteger ?
    currentTeam.channels.findIndex((item) => item.id === channelIdInteger) :
    0;
  const currentChannel = channelIdx === -1 ?
    currentTeam.channels[0] : currentTeam.channels[channelIdx];

  const currentChannelName = !currentChannel.dm ?
    currentChannel.name :
    getDmChName(currentChannel.pcMembers, currentTeam.members);

  const teamsProp = teams.map((t) => ({
    id: t.id,
    letter: t.name.charAt(0).toUpperCase(),
  }));

  return (
    <AppLayout className="app-layout">
      <Sidebar teams={teamsProp} username={me.username} currentTeam={currentTeam} />
      {currentChannel && <Header channelName={currentChannelName} />}
      {currentChannel && (
        <MessageContainer channelId={currentChannel.id} />
      )}
      <SendMessage
        onSubmit={async (text) => {
          await mutate({ variables: { text, channel_id: currentChannel.id } });
        }}
        placeholder={currentChannelName}
        channelId={currentChannel.id}
      />
    </AppLayout>
  );
};


const CREATE_MESSAGE_MUTATION = gql`
  mutation($channel_id: Int!, $text: String!) {
    createMessage(channel_id: $channel_id, text: $text)
  }
`;

export default compose(
  graphql(ME_QUERY, { options: { fetchPolicy: 'network-only' } }),
  graphql(CREATE_MESSAGE_MUTATION),
)(ViewTeam);
