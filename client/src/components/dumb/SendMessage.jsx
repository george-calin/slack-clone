import React from 'react';
import styled from 'styled-components';
import {
  TextArea, Form, Button, Icon,
} from 'semantic-ui-react';
import { withFormik } from 'formik';
import FileUpload from './FileUpload';


const styleTextArea = {
  fontSize: '16px',
  padding: '8px 12px',
  minHeight: '40px',
  maxHeight: '40px',
  borderRadius: '4px',
  borderWidth: '2px',
};

const styleUploadButton = {
  height: '100%',
  margin: 0,
};
const SendMessageWrapper = styled.div`
  grid-column: 3;
  grid-row: 3;
  padding: 10px;
  display: grid;
  grid-template-columns: 50px auto;
`;

const ENTER_KEY = 13;
const SendMessage = ({
  placeholder,
  open,
  onClose,
  values,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  channelId,
}) => (
  <SendMessageWrapper>
    <FileUpload noDrag={false} channelId={channelId}>
      <Button icon style={styleUploadButton}>
        <Icon name="plus" />
      </Button>
    </FileUpload>
    <Form>
      <TextArea
        rows={1}
        onChange={handleChange}
        onBlur={handleBlur}
        onKeyDown={(e) => {
          if (e.keyCode === ENTER_KEY) {
            handleSubmit(e);
          }
        }}
        style={styleTextArea}
        name="message"
        value={values.message}
        placeholder={`# ${placeholder}`}
      />
    </Form>
  </SendMessageWrapper>
);

export default withFormik({
  mapPropsToValues: () => ({ message: '' }),
  handleSubmit: async (
    values,
    {
      props: {
        onSubmit, onClose, channelId, mutate,
      }, setSubmitting, resetForm,
    },
  ) => {
    if (!values.message || !values.message.trim()) {
      setSubmitting(false);
      return;
    }
    await onSubmit(values.message);
    // await mutate({
    //   variables: { channel_id: channelId, text: values.message },
    // });
    // onClose();
    resetForm(false);
  },
})(SendMessage);
