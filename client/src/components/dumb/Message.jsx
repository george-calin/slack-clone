import React from 'react';
import { Comment } from 'semantic-ui-react';
import RenderText from './RenderText';

const MyMessage = ({ data: { url, text, filetype } }) => {
  if (url) {
    if (filetype.startsWith('image')) {
      return (
        <Comment.Content>
          <img src={url} alt="" style={{ maxWidth: '500px' }} />
        </Comment.Content>
      );
    } if (filetype === 'text/plain') {
      return (
        <Comment.Content>
          <RenderText url={url} />
        </Comment.Content>
      );
    } if (filetype.startsWith('audio/')) {
      return (
        <Comment.Content>
          <div>
            <audio controls>
              <source src={url} type={filetype} />
            </audio>
          </div>
        </Comment.Content>
      );
    }
  } else {
    return <Comment.Text>{text}</Comment.Text>;
  }
};

export default MyMessage;
