import React, { useCallback, useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import { graphql } from 'react-apollo';
import { gql } from 'apollo-boost';

const baseStyle = {
  borderStyle: 'none',
  outline: 'none',
  transition: '0.5s -webkit-filter linear',
};

// const activeStyle = {
// };

const acceptStyle = {
  WebkitFilter: 'blur(2px)',
  MozFilter: 'blur(2px)',
  OFilter: 'blur(2px)',
  msFilter: 'blur(2px)',
  filter: 'blur(2px)',
};

// const rejectStyle = {
// };

const FileUpload = ({
  children, noClick, noDrag, channelId, mutate
}) => {
  const onDrop = useCallback(async ([file]) => {
    const response = await mutate({
      variables: {
        channel_id: channelId,
        file,
      },
    });
    console.log(response);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const userDropzoneProps = {
    onDrop, className: 'ignore', noClick, noDrag, accept: 'text/*, application/*, image/*, audio/*',
  };

  const {
    getRootProps, getInputProps,
    // isDragActive,
    isDragAccept,
    // isDragReject,
  } = useDropzone(userDropzoneProps);

  const style = useMemo(() => ({
    ...baseStyle,
    // ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    // ...(isDragReject ? rejectStyle : {}),
  }), [
    // isDragActive,
    // isDragReject,
    isDragAccept,
  ]);

  return (
    <div {...getRootProps({ style })}>
      <input {...getInputProps()} />
      {children}
    </div>
  );
};


const CREATE_FILE_MESSAGE_MUTATION = gql`
mutation createMessage($channel_id: Int!, $file: Upload!) {
  createMessage(channel_id: $channel_id, file: $file) 
}
`;


export default graphql(CREATE_FILE_MESSAGE_MUTATION)(FileUpload);
