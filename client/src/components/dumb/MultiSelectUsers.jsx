/* eslint-disable react/prop-types */
import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import { graphql } from 'react-apollo';
import { TEAM_MEMBERS_QUERY } from '../../graphql/teams';

// eslint-disable-next-line max-len
const MultiSelectUsers = ({
  data: { loading, getTeamMembers = [] }, value, handleChange, placeholder,
}) => (loading ? null : (
  <Dropdown
    placeholder={placeholder}
    fluid
    multiple
    search
    selection
    value={value}
    onChange={handleChange}
    options={getTeamMembers.map((item) => ({ key: item.id, value: item.id, text: item.username }))}
  />
)
);

export default graphql(TEAM_MEMBERS_QUERY, {
  options: (props) => ({
    variables: {
      team_id: props.teamId,
    },
    fetchPolicy: 'network-only',
  }),
})(MultiSelectUsers);
