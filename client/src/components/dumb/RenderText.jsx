import React from 'react';

export default class RenderText extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }


  // eslint-disable-next-line camelcase
  UNSAFE_componentWillMount = async () => {
    const response = await fetch(this.props.url);
    const text = await response.text();
    this.setState({ text });
  }

  render() {
    const { text } = this.state;
    return (
      <div>
        <div>-----</div>
        <p>{text}</p>
        <div>-----</div>
      </div>
    );
  }
}
