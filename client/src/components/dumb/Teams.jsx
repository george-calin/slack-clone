import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const TeamsWrapper = styled.div`
  grid-column: 1;
  grid-row: 1 / 4;
  background-color: #362234;
  color: #958993;
`;

const TeamList = styled.ul`
  width: 100%;
  padding-left: 0px;
  list-style: none;
`;

const TeamListItem = styled.li`
  height: 50px;
  width: 50px;
  background-color: #676066;
  color: #fff;
  margin: auto;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  border-radius: 11px;
  &:hover {
    border-style: solid;
    border-width: thick;
    border-color: #767676;
  }
`;

const teamsMapFn = ({ id, letter }) => (
  <Link key={`link-team-${id}`} to={`/view-team/${id}`}>
    <TeamListItem key={`team-${id}`}>{letter}</TeamListItem>
  </Link>
);
const Teams = (props) => {
  const { teams } = props;
  return (
    <TeamsWrapper>
      <TeamList>
        {teams.map(teamsMapFn)}
        <Link key="add-team-btn" to="/create-team">
          <TeamListItem><FontAwesomeIcon icon={faPlus} size="1x" /></TeamListItem>
        </Link>
      </TeamList>
    </TeamsWrapper>
  );
};

export default Teams;
