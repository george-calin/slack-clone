import styled from 'styled-components';

export default styled.div`
  grid-column: 3;
  grid-row: 2;
  padding-left: 20px;
  padding-right: 20px;
  display: flex;
  flex-direction: column-reverse;
  overflow-y: auto;
  min-height: 0;
  min-width: 0;
`;
// background-color: #f2f2f2;
