/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import { faCircle, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { getDmChName } from '../../utils';

const ChannelWrapper = styled.div`
  grid-column: 2;
  grid-row: 1 / 4;
  background-color: #52364e;
  color: #958993;
`;

const TeamNameHeader = styled.h1`
  color: #fff;
  font-size: 20px;
`;

const SideBarList = styled.ul`
  width: 100%;
  list-style: none;
  padding-left: 0px;
  color: #bcabbc;
`;

const paddingLeft = 'padding-left: 10px';

const ClickyItem = styled.div`
  cursor: pointer;
`;
const SideBarListItem = styled.li`
  padding: 2px;
  ${paddingLeft};
  color: #bcabbc;
  &:hover {
    background: #3e313c;
  }
`;

const SideBarListHeader = styled.li`
  ${paddingLeft}
`;

const PushLeft = styled.div`
  ${paddingLeft}
`;

const Green = styled.span`
  color: #38978d;
`;

const Bubble = ({ on = true }) => (on ? (
  <Green>
    <FontAwesomeIcon icon={faCircle} size="xs" />
  </Green>
) : (
  'o'
));

const channelsMapFn = ({ id, name }, teamId) => (
  <Link key={`channel-${id}`} to={`/view-team/${teamId}/${id}`} style={{ textDecoration: 'none', color: '#bcabbc' }}>
    <SideBarListItem>
      #
      {' '}
      {name}
    </SideBarListItem>
  </Link>
);


const dmChannelMapFn = ({ id, name, pcMembers }, teamId, members) => (
  <Link key={`channel-${id}`} to={`/view-team/${teamId}/${id}`}>
    <SideBarListItem key={`user-${id}`}>
      <Bubble />
      {' '}
      {/* {name} */}
      {getDmChName(pcMembers, members)}

    </SideBarListItem>
  </Link>
);

const Channels = (props) => {
  const {
    teamName,
    teamId,
    isAdmin,
    username,
    members,
    channels,
    dmChannels,
    onAddChannelClick,
    onDirectMessageClick,
    onInvitePeopleClick,
  } = props;
  return (
    <ChannelWrapper>
      <PushLeft>
        <TeamNameHeader>{teamName}</TeamNameHeader>
        {username}
      </PushLeft>
      <div>
        <SideBarList>
          <SideBarListHeader>Channels</SideBarListHeader>
          {channels.map((channel) => channelsMapFn(channel, teamId))}
          {isAdmin && (
            <SideBarListItem key="add-channel" onClick={onAddChannelClick}>
              <ClickyItem>
                <FontAwesomeIcon icon={faPlus} size="xs" />
                {' '}
                Add Channel
              </ClickyItem>
            </SideBarListItem>
          )}
        </SideBarList>
      </div>
      <div>
        <SideBarList>
          <SideBarListItem onClick={onDirectMessageClick}>
            <ClickyItem>
              <FontAwesomeIcon icon={faPlus} size="xs" />
              {' '}
              Direct Messages
            </ClickyItem>
          </SideBarListItem>
          {dmChannels.map((channel) => dmChannelMapFn(channel, teamId, members))}
          {isAdmin && (
            <SideBarListItem key="invite-people" onClick={onInvitePeopleClick}>
              <ClickyItem>
                <FontAwesomeIcon icon={faPlus} size="xs" />
                {' '}
                Invite people
              </ClickyItem>
            </SideBarListItem>
          )}
        </SideBarList>
      </div>
    </ChannelWrapper>
  );
};

export default Channels;
