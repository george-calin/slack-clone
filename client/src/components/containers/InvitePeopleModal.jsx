import React from 'react';
import {
  Modal, Form, Input, Button,
} from 'semantic-ui-react';
import { withFormik } from 'formik';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';
import normalizeErrors from '../../utils/normalizeErrors';

const InvitePeopleModal = ({
  open,
  onClose,
  values,
  handleChange,
  handleSubmit,
  isSubmitting,
  touched,
  errors,
}) => (
  <Modal open={open} onClose={onClose} closeOnDimmerClick>
    <Modal.Header>Add People to your team</Modal.Header>
    <Modal.Content>
      <Form>
        <Form.Field required>
          <Input
            value={values.email}
            onChange={handleChange}
            name="email"
            fluid
            icon=""
            placeholder="User's email..."
          />
        </Form.Field>
        {touched.email && errors.email ? errors.email[0] : null}
      </Form>
    </Modal.Content>
    <Modal.Actions>
      <Button
        disabled={isSubmitting}
        onClick={onClose}
        negative
        content="Cancel"
      />
      <Button
        disabled={isSubmitting}
        onClick={handleSubmit}
        positive
        icon="plus"
        content="Add User"
        type="submit"
      />
    </Modal.Actions>
  </Modal>
);

const ADD_TEAM_MEMBER_MUTATION = gql`
  mutation($email: String!, $team_id: Int!) {
    addTeamMember(email: $email, team_id: $team_id) {
      ok
      errors {
        path
        message
      }
    }
  }
`;

export default compose(
  graphql(ADD_TEAM_MEMBER_MUTATION),
  withFormik({
    mapPropsToValues: () => ({ email: '' }),
    handleSubmit: async (
      values,
      { props: { onClose, teamId, mutate }, setSubmitting, setErrors },
    ) => {
      const response = await mutate({
        variables: { team_id: teamId, email: values.email },
      });
      const { ok, errors } = response.data.addTeamMember;
      if (ok) {
        onClose();
      } else {
        const filteredErrors = errors.filter((e) => e.message !== 'user_id must be unique');
        if (errors.length !== filteredErrors.length) {
          filteredErrors.push({
            path: 'email',
            message: 'This user is already part of the team',
          });
        }
        setErrors(normalizeErrors(filteredErrors));
      }
      setSubmitting(false);
    },
  }),
)(InvitePeopleModal);
