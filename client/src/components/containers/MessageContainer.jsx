/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
import { graphql } from 'react-apollo';
import { gql } from 'apollo-boost';
// import { useQuery } from '@apollo/react-hooks';
import { Comment } from 'semantic-ui-react';
// import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { FileUpload, Message } from '../dumb';

const MESSAGES_SUBSCRIPTION = gql`
  subscription($channel_id: Int!) {
    newChannelMessage(channel_id: $channel_id) {
      id
      text
      user {
        username
      }
      url
      filetype
      createdAt
    }
  }
`;
const LIMIT = 30;

class MessageContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasMoreItems: true,
    };
  }

  UNSAFE_componentWillMount() {
    this.unsubscribe = this.subscribe(this.props.channelId);
  }

  UNSAFE_componentWillReceiveProps({ channelId }) {
    if (this.props.channelId !== channelId) {
      if (this.unsubscribe) {
        this.unsubscribe();
      }
      this.unsubscribe = this.subscribe(channelId);
    }
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }

  subscribe = (channelId) => this.props.data.subscribeToMore({
    document: MESSAGES_SUBSCRIPTION,
    variables: {
      channel_id: channelId,
    },
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData) {
        return prev;
      }
      return {
        ...prev,
        messages: [subscriptionData.data.newChannelMessage, ...prev.messages],
      };
    },
  })

  fetchMoreMessages = async () => {
    const { data: { messages, fetchMore }, channelId } = this.props;
    await fetchMore({
      variables: {
        channel_id: channelId,
        cursor: messages[messages.length - 1].createdAt,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        if (fetchMoreResult.messages.length !== LIMIT) {
          this.setState({
            hasMoreItems: false,
          });
        }
        return { ...prev, messages: [...prev.messages, ...fetchMoreResult.messages] };
      },
    });
  }

  handleScroll = (e) => {
    const { data: { messages } } = this.props;
    const { hasMoreItems } = this.state;
    if (
      this.scroller &&
      this.scroller.scrollTop < 100 &&
      messages.length >= LIMIT &&
      hasMoreItems
    ) {
      this.fetchMoreMessages();
    }
  }

  render() {
    const { data: { loading, messages }, channelId } = this.props;
    const { hasMoreItems } = this.state;
    return (loading || messages === undefined) ? null : (
      <div
        id="scroll_wrapper"
        style={{
          gridColumn: 3,
          gridRow: 2,
          paddingLeft: '20px',
          paddingRight: '20px',
          display: 'flex',
          flexDirection: 'column-reverse',
          overflowY: 'auto',
          minHeight: 0,
          minWidth: 0,
        }}
        ref={(ref) => { this.scrollParentRef = ref; }}
      >
        <InfiniteScroll
          pageStart={0}
          loadMore={this.fetchMoreMessages}
          hasMore={messages.length >= LIMIT && hasMoreItems}
          loader={<h4 className="loader" key={0}>Loading ...</h4>}
          useWindow={false}
          getScrollParent={() => this.scrollParentRef}
          isReverse
        >
          <FileUpload
            noClick
            channelId={channelId}
          >
            <Comment.Group>
              {[...messages].reverse().map((m) => (
                <Comment key={`${m.id}-message`}>
                  <Comment.Content>
                    <Comment.Author as="a">{m.user[0].username}</Comment.Author>
                    <Comment.Metadata>
                      <div>{moment.unix((1 * m.createdAt) / 1000).format('HH:mm:ss DD/MM/YYYY')}</div>
                    </Comment.Metadata>
                    <Message data={m} />
                    <Comment.Actions>
                      <Comment.Action>Reply</Comment.Action>
                    </Comment.Actions>
                  </Comment.Content>
                </Comment>
              ))}
            </Comment.Group>
          </FileUpload>
        </InfiniteScroll>
      </div>
    );
  }
}

const MESSAGES_QUERY = gql`
query($cursor: String, $channel_id: Int!) {
  messages(cursor: $cursor, channel_id: $channel_id) {
    id
    text
    user {
        username
    }
    url
    filetype
    createdAt 
  }   
}
`;

export default graphql(MESSAGES_QUERY, {
  options: (props) => ({
    fetchPolicy: 'network-only',
    variables: {
      channel_id: props.channelId,
    },
  }),
})(MessageContainer);
