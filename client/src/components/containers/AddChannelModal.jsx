/* eslint-disable react/prop-types */
import React from 'react';
import {
  Modal, Form, Input, Button, Checkbox,
} from 'semantic-ui-react';
import { withFormik } from 'formik';
import { gql } from 'apollo-boost';
import { graphql } from 'react-apollo';
import { compose } from 'recompose';

import { ME_QUERY } from '../../graphql/teams';
import { MultiSelectUsers } from '../dumb';

const AddChannelModal = ({
  open,
  onClose,
  values,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  resetForm,
  setFieldValue,
  teamId,
}) => (
  <Modal
    open={open}
    onClose={(e) => {
      resetForm();
      onClose(e);
    }}
    closeOnDimmerClick
  >
    <Modal.Header>Add Channel</Modal.Header>
    <Modal.Content>
      <Form>
        <Form.Field required>
          <Input
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            name="name"
            fluid
            placeholder="Name..."
          />
        </Form.Field>
        <Form.Field>
          <Checkbox
            label="Private"
            toggle
            checked={!values.public}
            onChange={(e, { checked }) => setFieldValue('public', !checked)}
          />
        </Form.Field>
        {
          !values.public && (
            <Form.Field>
              <MultiSelectUsers
                placeholder="Select Members"
                value={values.members}
                teamId={teamId}
                handleChange={(e, { value }) => setFieldValue('members', value)}
              />
            </Form.Field>
          )
        }
      </Form>
    </Modal.Content>
    <Modal.Actions>
      <Button
        disabled={isSubmitting}
        onClick={(e) => {
          resetForm();
          onClose(e);
        }}
        negative
        content="Cancel"
      />
      <Button
        disabled={isSubmitting}
        onClick={handleSubmit}
        positive
        icon="plus"
        content="Submit"
        type="submit"
      />
    </Modal.Actions>
  </Modal>
);

const CREATE_CHANNEL_MUTATION = gql`
  mutation($team_id: Int!, $name: String!, $public: Boolean, $members: [Int!]) {
    createChannel(team_id: $team_id, name: $name, public: $public, members: $members) {
      ok
      channel {
        id
        name
        dm
      }
      errors {
        path
        message
      }
    }
  }
`;

export default compose(
  graphql(CREATE_CHANNEL_MUTATION),
  withFormik({
    mapPropsToValues: () => ({ name: '', public: true, members: [] }),
    handleSubmit: async (
      values,
      { props: { onClose, teamId, mutate }, setSubmitting, resetForm },
    ) => {
      await mutate({
        variables: {
          team_id: teamId,
          name: values.name,
          public: values.public,
          members: values.members,
        },
        optimisticResponse: {
          createChannel: {
            __typename: 'Mutation',
            ok: true,
            channel: {
              __typename: 'Channel',
              id: -1,
              name: values.name,
              dm: false,
            },
            errors: [
            ],
          },
        },
        update: (store, { data: { createChannel } }) => {
          const { ok, channel } = createChannel;
          if (!ok) {
            return;
          }
          const data = store.readQuery({ query: ME_QUERY });
          const { teams } = data.me;
          const teamIdx = teams.findIndex((item) => item.id === teamId);
          teams[teamIdx].channels.push(channel);
          store.writeQuery({ query: ME_QUERY, data });
        },
      });
      resetForm();
      onClose();
    },
  }),
)(AddChannelModal);
