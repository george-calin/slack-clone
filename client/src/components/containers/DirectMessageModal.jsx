/* eslint-disable react/prop-types */
import React from 'react';
import {
  Modal, Form, Button,
} from 'semantic-ui-react';
import { compose } from 'recompose';
import { graphql } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { withFormik } from 'formik';
import { gql } from 'apollo-boost';
import { MultiSelectUsers } from '../dumb';
import { ME_QUERY } from '../../graphql/teams';

const DirectMessageModal = ({
  open,
  onClose,
  teamId,
  setFieldValue,
  values,
  handleSubmit,
  isSubmitting,
  resetForm,
}) => (
  <Modal open={open} onClose={onClose} closeOnDimmerClick>
    <Modal.Header>Direct Messaging</Modal.Header>
    <Modal.Content>
      <Form>
        <Form.Field required>
          <MultiSelectUsers
            placeholder="Select members to message"
            value={values.members}
            teamId={teamId}
            handleChange={(e, { value }) => setFieldValue('members', value)}
          />
        </Form.Field>
      </Form>
    </Modal.Content>
    <Modal.Actions>
      <Button
        onClick={(e) => {
          resetForm();
          onClose(e);
        }}
        negative
        disabled={isSubmitting}
        content="Cancel"
      />
      <Button
        onClick={handleSubmit}
        primary
        disabled={isSubmitting}
        content="Start Messaging"
        type="submit"
      />
    </Modal.Actions>
  </Modal>
);

const CREATE_CHANNEL_DM_MUTATION = gql`
  mutation($team_id: Int!, $members: [Int!]) {
    createChannelDM(team_id: $team_id, members: $members) {
      id
      name
    } 
  }
`;

export default compose(
  withRouter,
  // graphql(TEAM_MEMBERS_QUERY, {
  //   options: (props) => ({
  //     variables: {
  //       team_id: props.teamId,
  //     },
  //     fetchPolicy: 'network-only',
  //   }),
  // }),
  graphql(CREATE_CHANNEL_DM_MUTATION),
  withFormik({
    mapPropsToValues: () => ({ members: [] }),
    handleSubmit: async (
      values,
      {
        props: {
          history, onClose, teamId, mutate,
        }, setSubmitting, resetForm,
      },
    ) => {
      await mutate({
        variables: {
          team_id: teamId,
          members: values.members,
        },
        update: (store, { data: { createChannelDM } }) => {
          const { id, name } = createChannelDM;
          const data = store.readQuery({ query: ME_QUERY });
          const { teams } = data.me;
          const teamIdx = teams.findIndex((item) => item.id === teamId);
          const notInChannelList = teams[teamIdx].channels.every((channel) => channel.id !== id);
          if (notInChannelList) {
            teams[teamIdx].channels.push({
              __typename: 'Channel',
              id,
              name,
              dm: true,
            });
            store.writeQuery({ query: ME_QUERY, data });
          }
          resetForm();
          onClose();
          history.push(`/view-team/${teamId}/${id}`);
        },
      });
      // resetForm();
      // onClose();
    },
  }),
)(DirectMessageModal);
