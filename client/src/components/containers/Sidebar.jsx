/* eslint-disable import/no-cycle */
/* eslint-disable react/prop-types */
import React from 'react';

import Channels from '../dumb/Channels';
import Teams from '../dumb/Teams';
import { AddChannelModal, DirectMessageModal, InvitePeopleModal } from '.';

class Sidebar extends React.Component {
  constructor() {
    super();
    this.state = {
      openAddChannelModal: false,
      openDirectMessageModal: false,
      openInvitePeopleModal: false,
    };
  }

  toggleAddChannelModal = (e) => {
    if (e) {
      e.preventDefault();
    }
    this.setState((prevState) => ({
      openAddChannelModal: !prevState.openAddChannelModal,
    }));
  };

  toggleDirectMessageModal = (e) => {
    if (e) {
      e.preventDefault();
    }
    this.setState((prevState) => ({
      openDirectMessageModal: !prevState.openDirectMessageModal,
    }));
  };

  toggleInvitePeopleClick = (e) => {
    if (e) {
      e.preventDefault();
    }
    this.setState((prevState) => ({
      openInvitePeopleModal: !prevState.openInvitePeopleModal,
    }));
  };


  render() {
    const { teams, currentTeam, username } = this.props;
    const { openAddChannelModal, openInvitePeopleModal, openDirectMessageModal } = this.state;
    const { toggleInvitePeopleClick, toggleAddChannelModal, toggleDirectMessageModal } = this;
    const regularChannels = [];
    const dmChannels = [];

    currentTeam.channels.forEach((c) => {
      if (c.dm) {
        dmChannels.push(c);
      } else {
        regularChannels.push(c);
      }
    });
    return (
      <>
        <Teams key="team-sidebar" teams={teams} />
        <Channels
          key="channels-sidebar"
          teamName={currentTeam.name}
          username={username}
          teamId={currentTeam.id}
          members={currentTeam.members}
          channels={regularChannels}
          isAdmin={currentTeam.isAdmin}
          dmChannels={dmChannels}
          onAddChannelClick={toggleAddChannelModal}
          onDirectMessageClick={toggleDirectMessageModal}
          onInvitePeopleClick={toggleInvitePeopleClick}
        />
        <AddChannelModal
          key="sidebar-add-channel-modal"
          open={openAddChannelModal}
          onClose={toggleAddChannelModal}
          teamId={currentTeam.id}
        />
        <DirectMessageModal
          key="sidebar-direct-message-modal"
          open={openDirectMessageModal}
          onClose={toggleDirectMessageModal}
          teamId={currentTeam.id}
        />
        <InvitePeopleModal
          key="sidebar-invite-people-modal"
          open={openInvitePeopleModal}
          onClose={toggleInvitePeopleClick}
          teamId={currentTeam.id}
        />
      </>
    );
  }
}

export default Sidebar;
